import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
    id("org.openjfx.javafxplugin") version  "0.0.9"
    id("application")
}

buildscript {
    val fxlauncherGradlePluginVersion = "1.0.12"
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("no.tornado", "fxlauncher-gradle-plugin", fxlauncherGradlePluginVersion)
    }
}

repositories {
    mavenCentral()
    jcenter()
    maven("http://oss.sonatype.org/content/repositories/snapshots")
    mavenLocal()
    maven(url = "https://jitpack.io")
    maven(url = "https://plugins.gradle.org/m2/")
}


dependencies {
    val jmetroVersion: String by project
    val tornadofxVersion: String by project

    implementation("org.jfxtras", "jmetro", jmetroVersion)
    implementation("no.tornado", "tornadofx", tornadofxVersion)
    implementation("de.gsi.chart", "chartfx-chart", "dev-x.2.0-SNAPSHOT")
//    implementation("org.controlsfx","controlsfx", "11.0.2")
}

application {
    mainClassName = "at.fkasper.mvp_chart_fx_231.DemoApp"
    applicationDefaultJvmArgs = listOf(
        "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED",
        "--add-exports=javafx.controls/com.sun.javafx.scene.control.inputmap=ALL-UNNAMED",
        "--add-exports=javafx.graphics/com.sun.javafx.scene.traversal=ALL-UNNAMED",
        "--add-opens=javafx.graphics/javafx.scene=ALL-UNNAMED",
        "--add-opens=javafx.graphics/javafx.scene=tornadofx"
    )

}

javafx {
    val javafxVersion: String by project
    version = javafxVersion
    modules = listOf("javafx.controls")
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

