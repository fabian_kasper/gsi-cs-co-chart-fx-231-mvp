This is a MVP to reproduce https://github.com/GSI-CS-CO/chart-fx/issues/231

I am using [JMetro](https://www.pixelduke.com/java-javafx-theme-jmetro/)'s dark theme. It turns out, that enabling JMetro like this

```kotlin
val jMetro = JMetro(root, Style.DARK)
root.styleClass.add(JMetroStyleClass.BACKGROUND);
```

triggers this bug. But unfortunately I have no clue why that happens. :(