package at.fkasper.mvp_chart_fx_231

import javafx.application.Application
import javafx.stage.Stage
import tornadofx.App
import tornadofx.InternalWindow

class DemoApp : App(MainView::class, InternalWindow.Styles::class) {
    override fun start(stage: Stage) {
        stage.width = 1000.0
        stage.height = 600.0
        super.start(stage)
    }
}


fun main(vararg args: String) {
    Application.launch(DemoApp::class.java, *args)
}
