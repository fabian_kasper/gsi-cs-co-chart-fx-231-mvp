package at.fkasper.mvp_chart_fx_231

import de.gsi.chart.XYChart
import de.gsi.chart.axes.spi.DefaultNumericAxis
import de.gsi.chart.plugins.CrosshairIndicator
import de.gsi.chart.plugins.Zoomer
import de.gsi.dataset.spi.DoubleDataSet
import javafx.scene.layout.Priority
import jfxtras.styles.jmetro.JMetro
import jfxtras.styles.jmetro.JMetroStyleClass
import jfxtras.styles.jmetro.Style
import tornadofx.*
import kotlin.math.cos
import kotlin.math.sin


class MainView : View("MVP for https://github.com/GSI-CS-CO/chart-fx/issues/231") {

    private val N_SAMPLES = 100 // default number of data points

    override val root = hbox {

        label("Lorem Ipsum")

        val chart = buildChart()

        // here we apply the "custom" css which tweaks the dark-theme of the chart
        val chartStylesheet = resources["/dark-chart.css"]
        chart.stylesheets.add(chartStylesheet)

        chart.apply {
            hgrow = Priority.ALWAYS
            useMaxWidth = true
            // add the chart to the vbox
        }
        add(chart)


    }


    init {
        // apply jmetro theme

        // ***********************
        // Un-commenting the next 2 lines triggers https://github.com/GSI-CS-CO/chart-fx/issues/231
        // ***********************

//        val jMetro = JMetro(root, Style.DARK)
//        root.styleClass.add(JMetroStyleClass.BACKGROUND);
    }

    /**
     * This is copied and converted to kotlin from
     * https://github.com/GSI-CS-CO/chart-fx/blob/master/chartfx-samples/src/main/java/de/gsi/chart/samples/SimpleChartSample.java
     */
    private fun buildChart(): XYChart {
        val chart = XYChart(DefaultNumericAxis(), DefaultNumericAxis())
        chart.plugins.add(Zoomer()) // standard plugin, useful for most cases
        chart.plugins.add(CrosshairIndicator())

        val dataSet1 = DoubleDataSet("data set #1")
        val dataSet2 = DoubleDataSet("data set #2")


        // lineChartPlot.getDatasets().add(dataSet1); // for single data set

        // lineChartPlot.getDatasets().add(dataSet1); // for single data set
        chart.datasets.addAll(dataSet1, dataSet2) // two data sets


        val xValues = DoubleArray(N_SAMPLES)
        val yValues1 = DoubleArray(N_SAMPLES)
        val yValues2 = DoubleArray(N_SAMPLES)
        // dataSet2.setAutoNotification(false); // to suppress auto notification
        // dataSet2.setAutoNotification(false); // to suppress auto notification
        for (n in 0 until N_SAMPLES) {
            val x = n.toDouble()
            val y1 = cos(Math.toRadians(10.0 * n))
            val y2 = sin(Math.toRadians(10.0 * n))
            xValues[n] = x
            yValues1[n] = y1
            yValues2[n] = y2
            dataSet2.add(n.toDouble(), y2) // style #1 how to set data, notifies re-draw for every 'add'
        }
        dataSet1[xValues] = yValues1 // style #2 how to set data, notifies once per set


        return chart
    }

}

